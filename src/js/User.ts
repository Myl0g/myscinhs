// This snippet implements the find method in arrays if it does not exist already.
if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this == null) {
      throw new TypeError("Array.prototype.find called on null or undefined");
    }
    if (typeof predicate !== "function") {
      throw new TypeError("predicate must be a function");
    }
    const list = Object(this);
    const length = list.length >>> 0;
    const thisArg = arguments[1];
    let value;

    for (let i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}

export default class User {
  public static MAX_ABSENCES: number = 3;
  public static COMPLETED_SERVICE_INDEX: number = 3; // last pos to first pos, starting @ 1
  public static SERVICE_REQUIREMENT_INDEX: number = 2; // last pos to first pos, starting @ 1

  // Interprets a JSON string containing details needed to create a User object.
  public static deserialize(str: string): User {
    const obj = JSON.parse(str);
    return new User(
      obj.ID,
      obj.first,
      obj.rollObject,
      obj.serviceObject,
      obj.namesObject,
    );
  }

  public ID: string;
  public first: string;
  public rollObject: any;
  public serviceObject: any;
  public namesObject: string[];

  constructor(
    ID: string,
    first: string,
    rollObject: string[],
    serviceObject: string[],
    namesObject: string[],
    roll: string[][] = null,
    service: string[][] = null,
  ) {
    this.ID = ID;
    this.first = first;

    if (roll === null && service === null) {
      this.rollObject = rollObject;
      this.serviceObject = serviceObject;
    } else {
      this.rollObject = roll.find((o: string[]) => o[0] === ID);
      this.serviceObject = service.find((o) => o[0] === ID);
    }

    this.namesObject = namesObject;

    if (typeof this.rollObject === "undefined") {
      document.cookie = "id=NONE;path=/";
      window.location.replace("/invalid");
    }
  }

  get serialized(): string {
    return JSON.stringify(this);
  }

  get name(): string {
    // In the CSV, names are stored as Last, First.
    return `${this.rollObject[2]} ${this.rollObject[1]}`;
  }

  get absences(): number {
    let prior: number = 0;

    this.rollObject.forEach((element: string) => {
      if (element.includes("Absent")) {
        prior = parseInt(element.replace(/^\D+/g, ""), 10);
      }
    });

    return prior;
  }

  get serviceNumber(): number {
    return parseInt(
      this.serviceObject[
        this.serviceObject.length - User.COMPLETED_SERVICE_INDEX
      ],
      10,
    );
  }

  get absencesLeft(): number {
    let requirement: number;

    if (this.newMember) {
      requirement = 2;
    } else {
      requirement = 3;
    }

    const result: number = requirement - this.absences; // 3 = number of maximum absences
    if (result > 0) {
      return result;
    }
    return 0;
  }

  get serviceLeft(): number {
    const result: number =
      parseInt(
        this.serviceObject[
          this.serviceObject.length - User.SERVICE_REQUIREMENT_INDEX
        ],
        10,
      ) - this.serviceNumber;
    if (result > 0) {
      return result;
    }
    return 0;
  }

  get completedEvents(): string[] {
    const res: string[] = [];
    let i = 0;
    for (const eventCreditGiven of this.serviceObject) {
      if (i < 4) {
        i++;
        continue;
      }

      if (
        this.namesObject[i] === "Total" ||
        this.namesObject[i] === "Requirement" ||
        this.namesObject[i] === "Met?"
      ) {
        i++;
        continue;
      }

      if (eventCreditGiven) {
        res.push(this.namesObject[i]);
      }
      i++;
    }
    return res;
  }

  get completedEventsAsHTML(): string {
    let res = "<ul>";
    for (const eventName of this.completedEvents) {
      res += `\n\t<li>${eventName}</li>`;
    }
    return res + "\n</ul>";
  }

  get probation(): boolean {
    return this.rollObject[4] === "TRUE";
  }

  get contest(): boolean {
    return this.rollObject[5] === "TRUE";
  }

  get newMember(): boolean {
    return this.rollObject[6] === "TRUE";
  }

  get duesPaid(): boolean {
    return this.rollObject[7] === "TRUE" || !this.newMember;
  }
}
