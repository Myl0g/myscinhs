import User from "./User";

// Finds the value of a certain cookie because browsers return a string containing all cookies and not an object
function getCookie(cookiename: string) {
  // Get name followed by anything except a semicolon
  const cookiestring = RegExp("" + cookiename + "[^;]+").exec(document.cookie);
  // Return everything after the equal sign, or an empty string if the cookie name not found
  return decodeURIComponent(
    !!cookiestring ? cookiestring.toString().replace(/^[^=]+./, "") : "",
  );
}

// Waits for arguments to resolve, then makes a new user from the stored cookies and resolved promises
export default async function getUser(
  first: string,
  roll: Promise<Response>,
  service: Promise<Response>,
  eventNames: Promise<Response>,
): Promise<User> {
  const rollResolved =  await (await roll).clone().json();
  const serviceResolved = await (await service).clone().json();
  const eventNamesResolved = await (await eventNames).clone().json();

  console.log(`Resolved roll as ${rollResolved}`);
  console.log(`Resolved service as ${serviceResolved}`);

  return new User(
    getCookie("id"),
    first,
    rollResolved,
    serviceResolved,
    eventNamesResolved,
  );
}
